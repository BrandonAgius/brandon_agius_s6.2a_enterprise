﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using BloggingWebsite.Models;
using Microsoft.AspNet.Identity;


namespace BloggingWebsite.Controllers
{
    [Authorize]
    public class BlogModelsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: BlogModels
        public ActionResult Index()
        {
           return View(db.Blog.ToList());
 
       }

        public ActionResult Home()
        {
            List<BlogModel> blgmdl = new List<BlogModel>(db.Blog.OrderByDescending(dte => dte.PostAdded)
                .Include(usr => usr.User).Take(5).ToList());
            ViewBag.blgmdl = blgmdl;

           
            return View();
        }


        // GET: BlogModels/Details/5
        public ActionResult Details(string id)
        {
            int userID;

            if (id == null)
            {
                return RedirectToAction("Home", "BlogModels");
            }
            else
            {
                userID = Convert.ToInt32(id);
                BlogModel blgDetail = new BlogModel();
                blgDetail = db.Blog.Include(usr => usr.User).FirstOrDefault(usrID => usrID.ID == userID);
                ViewBag.blgDetail = blgDetail;
                return View();
            }

           
        }

        [Route("BlogModels/Category/{id}")]
        public ActionResult Category(string id)
        {
            string categoryName = "";

            if (id == null)
            {
                return RedirectToAction("Home", "BlogModels");
            }
            else
            {
                List<BlogModel> blgCategory = new List<BlogModel>();
                switch(id)
                {
                    case "Javascript": blgCategory = db.Blog.Include(usr => usr.User).Where(cat => cat.Category.CategoryName == "Javascript").OrderByDescending(post => post.PostAdded).ToList();
                        categoryName = "Javascript"; 
                        break;
                    case "CSharp": blgCategory = db.Blog.Include(usr => usr.User).Where(cat => cat.Category.CategoryName == "C#").OrderByDescending(post => post.PostAdded).ToList();
                        categoryName = "C#";   
                        break;
                    case "CSS": blgCategory = db.Blog.Include(usr => usr.User).Where(cat => cat.Category.CategoryName == "CSS").OrderByDescending(post => post.PostAdded).ToList();
                        categoryName = "CSS";  
                        break;
                    case "MVC": blgCategory = db.Blog.Include(usr => usr.User).Where(cat => cat.Category.CategoryName == "MVC.NET").OrderByDescending(post => post.PostAdded).ToList();
                        categoryName = "MVC";  
                        break;
                    default: return RedirectToAction("Home", "BlogModels");
                        

                }
                ViewBag.blgCategory = blgCategory;
                ViewBag.categoryName = categoryName;

                return View();
            }


        }

        [Route("BlogModels/UserProfile/{id}")]
        public ActionResult UserProfile(string id)
        {
            string userName = "";
            if (id == null)
            {
                return RedirectToAction("Home", "BlogModels");
            }
            else
            {
                List<BlogModel> blgAuthor = new List<BlogModel>();

                blgAuthor = db.Blog.Include(usr => usr.User).Where(auth => auth.User.Name == id).OrderByDescending(post => post.PostAdded).ToList();
                
                ViewBag.blgAuthor = blgAuthor;
                ViewBag.Author = blgAuthor[0].User.Name;     
                return View();
            }


        }


        private List<SelectListItem> GetCategoryList()
        {
            return db.Category
              .Select(e => new SelectListItem
              {
                  Value = e.ID.ToString(),
                  Text = e.CategoryName
              })
             .ToList();
        }


        // GET: BlogModels/Create
        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(GetCategoryList(), "Value", "Text");
            return View();
        }

        // POST: BlogModels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ArticleHeading,Tags,Content")] BlogModel blogModel, String Category, HttpPostedFileBase ImageURL)
        {
           
                //ApplicationUser u = new ApplicationUser();

                var logedUserID = User.Identity.GetUserId();
                blogModel.User = db.Users.FirstOrDefault(u => u.Id == logedUserID);
                blogModel.Category = db.Category.FirstOrDefault(c => c.CategoryName == Category);
                blogModel.PostAdded = System.DateTime.Now;

            if (ImageURL != null)
                {
                    string imageName = Path.GetRandomFileName();
                    blogModel.ImageURL = "\\Pics\\" + imageName + Path.GetExtension(ImageURL.FileName);

                    ImageURL.SaveAs(Path.Combine(Server.MapPath("~/Pics/"), imageName + Path.GetExtension(ImageURL.FileName)));
                }

                db.Blog.Add(blogModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            

            return View(blogModel);
        }

        // GET: BlogModels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogModel blogModel = db.Blog.Find(id);
            if (blogModel == null)
            {
                return HttpNotFound();
            }
            return View(blogModel);
        }

        // POST: BlogModels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ArticleHeading,Tags,ImageURL,Content")] BlogModel blogModel)
        {
            if (ModelState.IsValid)
            {
                blogModel.PostAdded = System.DateTime.Now;
                db.Entry(blogModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(blogModel);
        }

        // GET: BlogModels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BlogModel blogModel = db.Blog.Find(id);
            if (blogModel == null)
            {
                return HttpNotFound();
            }
            return View(blogModel);
        }

        // POST: BlogModels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BlogModel blogModel = db.Blog.Find(id);
            db.Blog.Remove(blogModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //GOOGLE DRIVE SECTION
        [HttpGet]
        public ActionResult GetGoogleDriveFiles()
        {
            return View(GoogleDriveRepo.GetDriveFiles());
        }

        [HttpPost]
        public ActionResult DeleteFile(GoogleDrive file)
        {
            GoogleDriveRepo.DeleteFile(file);
            return RedirectToAction("GetGoogleDriveFiles");
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            GoogleDriveRepo.FileUpload(file);
            return RedirectToAction("GetGoogleDriveFiles");
        }

        public void DownloadFile(string id)
        {
            string FilePath = GoogleDriveRepo.DownloadGoogleFile(id);


            Response.ContentType = "application/zip";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(System.Web.HttpContext.Current.Server.MapPath("~/GoogleDrive/" + Path.GetFileName(FilePath)));
            Response.End();
            Response.Flush();
        }
    }


}

