﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BloggingWebsite.Models
{
    public class BlogModel
    {
        
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string ArticleHeading { get; set; }
        //public int CategoryModelID { get; set; }

        [Required]
        public string Tags { get; set; }

        [Required]
        public String ImageURL { get; set; }

        [Required]
        public string Content { get; set; }
        public string ContentTrimmed { get; set; }
        public DateTime PostAdded { get; set; }
        //public string AuthorID { get; set; }

       // public int Category_ID { get; set; }
 
       
        public virtual ApplicationUser User { get; set; }

       
        public virtual CategoryModel Category { get; set; }

    }
}