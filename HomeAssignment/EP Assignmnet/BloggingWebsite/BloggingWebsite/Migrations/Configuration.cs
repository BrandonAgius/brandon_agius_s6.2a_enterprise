namespace BloggingWebsite.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BloggingWebsite.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "BloggingWebsite.Models.ApplicationDbContext";
        }

        protected override void Seed(BloggingWebsite.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
           /* List<Models.CategoryModel> categories = new List<Models.CategoryModel>
            {
                new Models.CategoryModel{CategoryName="C#"},
                new Models.CategoryModel{CategoryName="Javascript"},
                new Models.CategoryModel{CategoryName="MVC.NET"},
                new Models.CategoryModel{CategoryName="CSS"}
            };

            categories.ForEach(c => context.Category.Add(c));
            context.SaveChanges();*/
           



        }
    }
}
