namespace BloggingWebsite.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlogModels",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ArticleHeading = c.String(),
                        Tags = c.String(),
                        ImageURL = c.String(),
                        Content = c.String(),
                        User_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.User_Id)
                .Index(t => t.User_Id);
            
            AddColumn("dbo.AspNetUsers", "Name", c => c.String());
            AddColumn("dbo.AspNetUsers", "LastName", c => c.String());
            AddColumn("dbo.AspNetUsers", "ProfileDescription", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BlogModels", "User_Id", "dbo.AspNetUsers");
            DropIndex("dbo.BlogModels", new[] { "User_Id" });
            DropColumn("dbo.AspNetUsers", "ProfileDescription");
            DropColumn("dbo.AspNetUsers", "LastName");
            DropColumn("dbo.AspNetUsers", "Name");
            DropTable("dbo.BlogModels");
        }
    }
}
