﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BloggingWebsite.Startup))]
namespace BloggingWebsite
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
